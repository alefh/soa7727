package com.alefh.molejao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MolejaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(MolejaoApplication.class, args);
	}
}

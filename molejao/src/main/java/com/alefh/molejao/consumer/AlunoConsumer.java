package com.alefh.molejao.consumer;

import com.alefh.molejao.domain.Aluno;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
@Slf4j
public class AlunoConsumer {

    @RabbitListener(queues = "fila-aluno", containerFactory = "simpleRabbitListenerContainerFactory")
    public void leitorMensagens(Aluno aluno) {
        log.info("lendo msg {}", aluno);
    }
}

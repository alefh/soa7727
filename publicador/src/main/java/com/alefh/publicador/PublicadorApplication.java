package com.alefh.publicador;

import com.alefh.publicador.domain.Aluno;
import com.alefh.publicador.domain.Curso;
import com.alefh.publicador.rabbitmq.NotificaRabbitMQ;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@SpringBootApplication
@Slf4j
public class PublicadorApplication {

	@Autowired private NotificaRabbitMQ notificaRabbitMQ;

	public static void main(String[] args) {
		SpringApplication.run(PublicadorApplication.class, args);
	}


	@PostConstruct
	public void diaDaMaldade() {

		Integer contador = 0;
		Curso java = new Curso("36", "java", 40);
		Aluno aluno = new Aluno("123", "Jose", "soa7223", 30, "maria", java);
		while (contador <= 546000) {
			log.info("enviando a mensagem {}", contador);
			notificaRabbitMQ.notificaAluno(aluno);
			contador++;
		}
	}
}

package com.alefh.publicador.api.controllers;

import com.alefh.publicador.domain.Aluno;
import com.alefh.publicador.service.AlunoService;
import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;
import java.util.List;

@RestController("/alunos")
@AllArgsConstructor
public class AlunoController {

    private AlunoService alunoService;

    @GetMapping
    public List<Aluno> listaAlunos() {
        return alunoService.buscaTodos();
    }

    @PostMapping
    public ResponseEntity<Aluno> criaAluno(@RequestBody Aluno aluno) throws URISyntaxException {
        Aluno criaAluno = alunoService.criaAluno(aluno);
        return ResponseEntity.ok(criaAluno);

    }


}

package com.alefh.publicador.rabbitmq;

import com.alefh.publicador.domain.Aluno;
import lombok.extern.slf4j.Slf4j;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class NotificaRabbitMQ {

    @Value("${publicador.rabbitmq.topic.aluno}")
    private String topicoAluno;

    @Autowired private RabbitTemplate rabbitTemplate;

    public void notificaAluno(Aluno aluno) {
        log.info("enviando mensagem para o rabbit aluno: {} curso: {}", aluno.getNome(), aluno.getCurso().getNome());
        rabbitTemplate.convertAndSend(topicoAluno, "*", aluno);
    }
}

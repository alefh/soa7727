package com.alefh.publicador.service;

import com.alefh.publicador.domain.Aluno;
import com.alefh.publicador.rabbitmq.NotificaRabbitMQ;
import com.alefh.publicador.repositories.AlunoRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@AllArgsConstructor
public class AlunoService {

    private AlunoRepository repository;
    private NotificaRabbitMQ notificaRabbitMQ;

    public Aluno criaAluno(Aluno aluno) {
        Aluno save = repository.save(aluno);
        notificaRabbitMQ.notificaAluno(save);
        return save;
    }

    public Aluno atualizaAluno(Aluno aluno) {
        Aluno save = repository.save(aluno);
        notificaRabbitMQ.notificaAluno(save);
        return save;
    }

    public Aluno buscaAlunoPeloId(String id) {
        return repository.findOne(id);
    }

    public void deletaAluno(String id) {
        repository.delete(id);
    }

    public List<Aluno> buscaTodos() {
        return repository.findAll();
    }
}
